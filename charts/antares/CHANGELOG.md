# Change Log

## v3.7.0

### Added

* filterRunner deployment

## v3.6.0

### Added

* stampExtractor deployment

## v3.5.0

### Changed

* Index worker can access to a service account to authenticate to GCP services.

## v3.4.0

### Changed

* Verify-catalogs hooks supports Bigtable as database.


## v3.3.0

### Changed

* AN-1402_template_memory_limits: Make pipeline worker memory limits configurable from Values.yaml


## v3.2.0

### Added

* gravWaveWatcher deployment
